const InsulaDb = require("../models").Insula;
const CrocodilDb = require("../models").Crocodil;

const controller = {
  getAllIslands: async (req, res) => {
    InsulaDb.findAll()
      .then((islands) => {
        res.status(200).send(islands);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send({ message: "Server error!" });
      });
  },

  getIslandById: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "ID not provided" });
    }

    InsulaDb.findByPk(id)
      .then((insula) => {
        res.status(200).send({ insula });
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send({ message: "Server error!" });
      });
  },

  getCrocosFromIsland: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "ID not provided" });
    }

    InsulaDb.findByPk(id, {
      include: [{ model: CrocodilDb, as: "Crocodil" }],
    })
      .then((insula) => {
        res.status(200).send(insula);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send({ message: "Server error!" });
      });
  },

  addInsula: async (req, res) => {
    const insula = ({ nume, suprafata } = req.body);
    if (!nume || !suprafata) {
      res.status(400).send({ message: "Bad input" });
    } else {
      InsulaDb.create(insula)
        .then((ins) => {
          console.log("Created");
          res.status(201).send({ ins });
        })
        .catch((err) => {
          console.log(err);
          res.status(500).send({ message: "Server error!" });
        });
    }
  },
};

module.exports = controller;
